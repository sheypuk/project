import React, {Component} from 'react';
import s from './ProfileInfo.module.css';


class ProfileStatus extends React.Component {


    state = {
        editMode: false,
        status: this.props.status

    }

    activateEditNode = () => {
        this.setState({
            editMode: true
        })
    }
    deactivateEditNode = () => {
        this.setState({
            editMode: false
        })
        this.props.updateStatus(this.props.status);
    }


onStatusChange = (e) => {
       this.setState({
           status:e.currentTarget.value
       });
}

componentDidUpdate(prevProps, prevState) {
        if (prevProps.status !== this.props.status)

        this.setState({
            status: this.props.status})

}


    render() {

        return (
            <div>
                {!this.state.editMode &&
                <div>
                    <span onDoubleClick={this.activateEditNode}> {!this.props.status  || "--" }</span>
                </div>
                }
                {this.state.editMode &&
                <div>
                    <input onChange={this.onStatusChange} autoFocus={true} onBlur={this.deactivateEditNode}
                           value={this.state.status}/>
                </div>
                }
            </div>
        )
    }
}

export default ProfileStatus;
